var fs = require('fs');
var path = require('path');

var UglifyJS = require('uglify-js');

var sizes = {};

// Directory is always root of module
var entries = fs.readdirSync('.');
entries.forEach(function (filename) {
    if (!/\.js$/i.test(filename) || /\.min\.js$/.test(filename)) return;

    var sourceMap = filename + '.map';
    var minifiedName = filename.replace(/\.js$/, '.min.js');
    var minifiedFilename = filename.replace(/\.js$/, '.min.js');
    var sourceMapFilename = filename + '.map';

    var result = UglifyJS.minify(filename, {
        outSourceMap: sourceMap,
        mangle: true,
        mangleProperties: {regex: /^m[A-Z]/},
        compress: true
    });
    fs.writeFileSync(minifiedFilename, result.code);
    fs.writeFileSync(sourceMapFilename, result.map);
    var codeBuffer = new Buffer(result.code, 'utf-8');
    sizes[minifiedName] = codeBuffer.length;
    console.log((Array(25).join(' ') + minifiedName).substring(minifiedName.length) + ': ' + codeBuffer.length + ' bytes');
});

process.argv.slice(2).forEach(function (textFile) {
    var text = fs.readFileSync(textFile, {encoding: 'utf-8'});
    for (var name in sizes) {
        var parts = text.split(name);
        for (var i = 1; i < parts.length; i++) {
            var part = parts[i];
            part = part.replace(/^([a-zA-Z `"'()]+)[0-9]+/, function (match, prefix) {
                return prefix + sizes[name];
            });
            parts[i] = part;
        }
        text = parts.join(name);
    }
    fs.writeFileSync(textFile, text);
});
