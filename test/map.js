var UriTemplate = require('../');
var assert = require('chai').assert;

describe("Construct maps", function () {
	var forwardLength = 0, reverseLength = 0;
	var mapLength = 0, mapLengthCount = 0;

	it('with object', function () {
		var map = UriTemplate.Map({
			'/foo/{foo}/bar/{bar}': '/bar/{bar}/foo/{foo}'
		});
		forwardLength += map.forwardCode.length;
		reverseLength += map.reverseCode.length;
		mapLength += map.mapCode.length;
		mapLengthCount += 1;

		var preUrl = '/foo/F/bar/B', postUrl = '/bar/B/foo/F', noMatchUrl = 'foobar';
		assert.strictEqual(map.forward(preUrl), postUrl);
		assert.strictEqual(map.reverse(postUrl), preUrl);
		assert.equal(map.forward(noMatchUrl), null);
		assert.equal(map.reverse(noMatchUrl), null);
	});

	it('with array', function () {
		var map = UriTemplate.Map([
			{'/foo/{foo}/bar/{bar}': '/bar/{bar}/foo/{foo}'},
			{'/{+url}': '/prefix/{+url}'}
		]);
		forwardLength += map.forwardCode.length;
		reverseLength += map.reverseCode.length;
		mapLength += map.mapCode.length;
		mapLengthCount += 1;

		var preUrl = '/foo/F/bar/B', postUrl = '/bar/B/foo/F', noMatchUrl = 'foobar';
		assert.strictEqual(map.forward(preUrl), postUrl);
		assert.strictEqual(map.reverse(postUrl), preUrl);
		assert.equal(map.forward(noMatchUrl), null);
		assert.equal(map.reverse(noMatchUrl), null);

		assert.strictEqual(map.forward('/xyz'), '/prefix/xyz');
		assert.strictEqual(map.reverse('/prefix/xyz'), '/xyz');
	});

	it('with array (priority order)', function () {
		var map = UriTemplate.Map([
			{'/{+url}': '/prefix/{+url}'},
			{'/foo/{foo}/bar/{bar}': '/bar/{bar}/foo/{foo}'}
		]);
		forwardLength += map.forwardCode.length;
		reverseLength += map.reverseCode.length;
		mapLength += map.mapCode.length;
		mapLengthCount += 1;

		var preUrl = '/foo/F/bar/B', postUrl = '/bar/B/foo/F', noMatchUrl = 'foobar';
		assert.strictEqual(map.forward(preUrl), '/prefix' + preUrl);
		assert.strictEqual(map.reverse(postUrl), preUrl);
	});

	it('recognises named argument', function () {
		var map = UriTemplate.Map({'/{?foo}': '/foo/{foo}'});

		forwardLength += map.forwardCode.length;
		reverseLength += map.reverseCode.length;
		mapLength += map.mapCode.length;
		mapLengthCount += 1;

		var preUrl = '/?foo=bar', postUrl = '/foo/bar', noMatchUrl = 'foobar';
		assert.strictEqual(map.forward(preUrl), postUrl);
		assert.strictEqual(map.reverse(postUrl), preUrl);
	});

	it('lengths', function () {
		console.log(forwardLength, reverseLength, mapLength, mapLengthCount);
		var averageLength = (forwardLength + reverseLength)/2/mapLengthCount;
		console.log('\t\t' + averageLength + ' bytes/function');
		console.log('\t\t' + (mapLength/mapLengthCount) + ' bytes/map');
		if (averageLength > 1200) {
			throw new Error('map functions should be shorter than individual templates combined');
		}
		if (averageLength <= mapLength/2/mapLengthCount) {
			throw new Error('map expression should be shorter than individual map methods');
		}
	});
});
