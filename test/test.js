"use strict";

var uriTemplates = require('../');
var assert = require('chai').assert;

var fillLength = 0, fillLengthCount = 0;
var testLength = 0, testLengthCount = 0;
var guessLength = 0, guessLengthCount = 0;

describe("Basic tests", function () {
	it("Basic substitution", function () {
		var template = uriTemplates("/prefix/{var}/suffix");
		var uri = template.fill({var: "test"});

		assert.strictEqual(uri, "/prefix/test/suffix");
	});
});

var optionSets = [
	null,
	{object: true},
	{
		fixedTypes: true,
		_ignore: function (template) {
			return /[?;&](count|list|1337)\*/.test(template) || /[/\.](keys)\*/.test(template);
		}
	}
];

function createTests(title, examplesDoc) {
	describe(title + " (substitution)", function () {
		for (var sectionTitle in examplesDoc) {
			var exampleSet = examplesDoc[sectionTitle];
			describe(sectionTitle, function () {
				var variables = exampleSet.variables;
				var variableFunction = function (varName) {
					return variables[varName];
				};
				var variableExpressions = {};
				for (var varName in variables) {
					variableExpressions[varName] = JSON.stringify(variables[varName]);
				}

				for (var i = 0; i < exampleSet.testcases.length; i++) {
					var pair = exampleSet.testcases[i];

					(function (templateString, expected) {
						it(templateString, function () {
							optionSets.forEach(function (options) {
								// Limit behaviour
								if (options && options._ignore && options._ignore(templateString)) return;
								
								var template = uriTemplates(templateString, options);
								fillLength += template.fillCode.length;
								fillLengthCount++;
								testLength += template.testCode.length;
								testLengthCount++;

								var actualUri = template.fill(variables);
								if (typeof expected == "string") {
									assert.strictEqual(actualUri, expected);
								} else {
									assert.include(expected, actualUri, expected[0]);
								}
								if (!options || !options.object) {
									var functionUri = template.fill(variableFunction);
									assert.strictEqual(actualUri, functionUri, "fill(function)");
								}

								[].concat(expected).forEach(function (expected) {
									var isMatch = template.test(expected);
									assert.strictEqual(isMatch, true);
								});
							});
						});
					})(pair[0], pair[1]);
				}
			});
		}
	});

	var unguessable = {};

	describe(title + " (de-substitution)", function () {
		for (var sectionTitle in examplesDoc) {
			var exampleSet = examplesDoc[sectionTitle];
			describe(sectionTitle, function () {
				for (var i = 0; i < exampleSet.testcases.length; i++) {
					var pair = exampleSet.testcases[i];

					(function (templateString, expected, exampleSet) {
						if (unguessable[templateString]) {
							return;
						}

						it(templateString, function () {
							[].concat(expected).forEach(function (original) {
								optionSets.forEach(function (options) {
									if (options && options._ignore && options._ignore(templateString)) return;
									var template = uriTemplates(templateString, options);
									guessLength += template.fromUriCode.length;
									guessLengthCount++;

									var guessedVariables = template.fromUri(original);
									assert.isObject(guessedVariables, 'guess is object');
									var reconstructed = template.fill(guessedVariables);
									
									if (typeof expected == "string") {
										assert.strictEqual(reconstructed, expected, 'reconstruction matches');
									} else {
										assert.include(expected, reconstructed, 'reconstruction matches', expected[0]);
									}
								});
							});
						});
					})(pair[0], pair[1], exampleSet);
				}
			});
		}
	});
}

createTests("Spec examples by section", require('./uritemplate-test/spec-examples-by-section.json'));
createTests("Extended examples", require('./uritemplate-test/extended-tests.json'));

createTests("Custom examples 1", require('./custom-tests.json'));
createTests("Custom examples 2", require('./custom-tests-2.json'));

require('./custom-tests.js');

after(function () {
	console.log('Length:');
	console.log('\tfill: ' + fillLength/fillLengthCount);
	console.log('\tfromUri: ' + guessLength/guessLengthCount);
	console.log('\ttest: ' + testLength/testLengthCount);
});
