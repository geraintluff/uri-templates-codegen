(function (moduleName, factory) {
    if (typeof module === 'object' && module.exports) {
        module.exports = factory();
    } else if (typeof define === 'function' && define.amd) {
        define(moduleName, [], factory);
    } else {
        global[moduleName] = factory();
    }
}).call(this, 'uri-templates-codegen', function () {
    function regexEscape(str) {
        return str.replace(/[-\[\]/{}()*+?.\\^$|]/g, '\\$&');
    }

    var evalFunc = eval;

    /*
        Top-level variables:
            o: object for filling
            f: function for filling
            u: input URL for fromUri
            j: join function for filling
            p: parse function for fromUri
    */
    function commonPrefix(opts) {
        var fillCode = 'function(o){';
        if (opts.object) {
            fillCode += ''
        } else {
            fillCode += 'var f=typeof o=="function"?o:function(p){return o[p]};';
        }
        // join(actualValue, escape, prefix, joiner, keyJoiner, prefixVar, joinVar, includeEmpty, returnUndefined, undefined)
        var fillJoinCode = 'function j(a,x,p,j,k,r,o,e,u,U){'
        // escape(string, match, keepExclamations, encodePercents)
            + 'function E(s,m,X,p){'
            +         'return s.replace(m||/.*/,encodeURIComponent)'
            +             '.replace(/!/g,X?"!":"%21")'
            +             '.replace(/%25(..)/g,p?"$&":"%$1")'
            +     '}'
        // w is the filtered array, v is an array or object
            +     'var w=[],v=[].concat(a),i=0;'
            +     'if (a!=null)'
        // array/object check:
        //  if .concat(a) produces an array with "a" at index 0
        //  then a is either a non-array object, or an array with itself as the first index (at which point you deserve what you get)
            +         'if(typeof a=="object"&&v[0]===a)'
        // If joinVar is set (i.e. variable has '*' in it), remove all variable prefixes because the variables are taken from the object keys
            +             'for(i in o&&(o=r=0),a)'
            +                 'a[i]!=null&&(w.push(i),w.push(a[i]));'
            +         'else '
            +             'for(k=j;i<v.length;i++)'
            +                 'v[i]!=null&&w.push(v[i]);'
            +     ''
            +     'a=w.length?"":u?U:"";'
            +     'for(i=0;i<w.length;i++)'
            +         'a+=(i?i%2?k||j:j:p)||"",'
            +         'v=w[i]+"",'
        //  If:
        //      i is zero and we have a variable name set
        //      or: i is nonzero and we're meant to add the variable name before each entry
            +         '(i?o:r)'
        //  then add the variable name, with optional equals sign
            +             '&&(a+=E(r)+(v||e?"=":"")),'
            +         'a+=E(v,x?0:/[^-a-z_0-9,/=&?#:;!]/gi,!x,x)'
            +     ';'
            +     'return a'
            + '}'
        fillCode += fillJoinCode;

        var guessCode = 'function(u){';
        // parse(arrayMode, string, prefixLength, joinChar, escaped)
        var guessParseCode = 'function p(A,s,p,j,x,u){'
            +    's=s.substring(p).split(j);'
            +    'for(var o={},i=0,m,d=decodeURIComponent;o&&i<s.length;i++)'
            +        'A?'
        // Escape if needed
            +            'x&&(s[i]=d(s[i]))'
            +        ':'
        // Match to a pair
            +            's[i]&&(m=/^([^=]*)=?(.*)$/.exec(s[i]))&&'
        // Update object if non-empty entry found
            +                '(o[d(m[1])]=d(m[2]));'
            +    'return A?s.length>1?s:s[0]:o'
            + '}';
        guessCode += guessParseCode;

        var mapCode = 'function(u){';
        mapCode += fillJoinCode + guessParseCode;

        return {
            mFill: fillCode,
            mFromUri: guessCode,
            mMap: mapCode,
         mCommon: fillJoinCode + guessParseCode
        };
    }

    function returnCode(spec, opts, fillExpr) {
        var strict = opts.strict;
        var fillReturn = [];
        fillExpr = fillExpr || function(prop) {
            var parts = prop.split(':');
            var result;
            if (opts.object) {
                result = 'o[' + JSON.stringify(parts[0]) + ']';
            } else {
                result = 'f(' + JSON.stringify(parts[0]) + ')';
            }
            if (parts.length > 1) {
                result = result + '&&(' + result + '+"").substring(0,' + +parts[1] + ')';
            }
            return result
        };
        var guessMatch = '';
        var guessIndex = 1;
        var guessReturn = {};

        var specParts = spec.split('{');
        var firstPart = specParts.shift();
        if (firstPart) {
            fillReturn.push(JSON.stringify(firstPart));
            guessMatch += regexEscape(firstPart);
        }
        while (specParts.length) {
            var specPart = specParts.shift();
            var varSpec = specPart.split('}',1)[0];
            var textPart = specPart.substring(varSpec.length + 1);
            var preChars = {};
            var varNames = varSpec.replace(/^([+/;?&#.])/, function (match) {
                preChars[match] = match;
                return '';
            }).split(',');

            var greedyChar = '?';
            var urlValueMatch = '([^%/&?#:;!=]|';
            var urlPairMatch = '([^%/&?#:;!]|';
            if (strict) {
                urlValueMatch += '%[0-9a-fA-F][0-9a-fA-F]';
                urlPairMatch += '%[0-9a-fA-F][0-9a-fA-F]';
            } else {
                urlValueMatch += '%..';
                urlPairMatch += '%..';
            }
            urlValueMatch += ')*' + greedyChar;
            urlPairMatch += ')*' + greedyChar;

            var prefixChar = preChars['/'] || preChars[';'] || preChars['?'] || preChars['&'] || preChars['#'] || preChars['.'] || '';
            var varGroupJoinChar = preChars['/'] || preChars[';'] || (preChars['?'] && '&') || preChars['&'] || preChars['.'] || ',';
            var fillGroup = [];
            var singleEntry = varNames.length === 1;

            guessMatch += '(';
            guessIndex++;
            if (prefixChar) guessMatch += regexEscape(prefixChar);
            varNames.forEach(function (varName, varIndex) {
                var postChars = {};
                varName = varName.replace(/[*]$/, function (match) {
                    postChars[match] = match;
                    return '';
                });

                var joinChar = (postChars['*']
                    && (preChars['/'] || preChars[';'] || preChars['&'] || (preChars['?'] && '&') || preChars['.'])
                ) || ',';
                var escape = !preChars['+'] && !preChars['#'];
                var mightBeObject = escape && postChars['*'];
                var mightBeArray = true, mustBeArray = false;
                var prefixVar = 0, joinVar = false, includeEmpty = false;
                var hasDefaultValue = false;
                if (preChars[';'] || preChars['?'] || preChars['&']) {
                    mightBeArray = !mightBeObject || !opts.fixedTypes;
                    includeEmpty = preChars['?'] || preChars['&'];
                    prefixVar = mightBeArray ? varName.split(':')[0] : 0;
                    joinVar = postChars['*'];
                    hasDefaultValue = true;
		  }
		  if ((preChars['/'] || preChars['.']) && postChars['*'] && opts.fixedTypes) {
			  mightBeObject = false;
			  mustBeArray = true;
		  }
                var keyJoinChar = postChars['*'] ? '=' : joinChar;

                // If there's only one entry, then include the actual prefix and failure result
                var callExpr = 'j(' + fillExpr(varName) + ',' + (escape?1:0) + ',' + (singleEntry ? JSON.stringify(prefixChar) : '0') + ',' + JSON.stringify(joinChar) + ',' + JSON.stringify(keyJoinChar)
                    + ',' + JSON.stringify(prefixVar)
                    + ',' + (joinVar ? 1 : 0)
                    + ',' + (includeEmpty ? 1 : 0)
					+ ',' + (singleEntry ? 0 : 1) + ')';
                callExpr = callExpr.replace(/(,0)+\)$/, ')');
                fillGroup.push(callExpr);

                var prefixLength = (prefixVar ? prefixVar.length + 1 : 0);
                var joinMatch = regexEscape(joinChar);

                if (mightBeObject && mightBeArray) {
                    guessMatch += '(';
                    guessIndex++;
                }
                var callExpr = '';
                if (mightBeArray) {
                    var guessArrayExpr = 'm[' + guessIndex + ']';
                    guessMatch += '(';
                    guessIndex++;
                    if (varIndex > 0) {
                        guessMatch += regexEscape(varGroupJoinChar);
                        prefixLength++;
                    }

                    if (prefixVar) {
                        guessMatch += regexEscape(prefixVar) + '(=';
                        guessIndex++;
                    }
                    if (escape) {
                        guessMatch += mustBeArray ? urlPairMatch : urlValueMatch;
                        guessIndex += 1;
                    } else {
                        guessMatch += '.*' + greedyChar;
                    }
                    if (prefixVar) {
                        guessMatch += ')?';
                    }

                    if (postChars['*']) {
                        guessMatch += '(' + regexEscape(joinChar);
                        guessIndex++;

                        if (joinVar) {
                            joinMatch += regexEscape(prefixVar) + '=?';
                            guessMatch += regexEscape(prefixVar) + '(=';
                            guessIndex++;
                        }
                        if (escape) {
                            guessMatch += mustBeArray ? urlPairMatch : urlValueMatch;
                            guessIndex += 1;
                        } else {
                            guessMatch += '.*' + greedyChar;
                        }
                        if (joinVar) {
                            guessMatch += ')';
                        }

                        guessMatch += ')*' + greedyChar;
                    }
                    // TODO: how to handle case where first variable is missing?  Should not require ?&bar=x, and accept ?bar=x instead.
                    guessMatch += ')'; // end of array match
                    // Having a default value
                    if (varIndex > 0 || hasDefaultValue) {
                        guessMatch += '?';
                    }
                    callExpr += guessArrayExpr + '&&p(1,' + guessArrayExpr
                        + ',' + JSON.stringify(prefixLength)
                        + ',/' + joinMatch + '/'
                        + (escape ? ',1' : '') + ')';
                    callExpr = callExpr.replace(/(,0)+\)$/, ')');
		      if (mustBeArray) {
			      callExpr += '||[]';
		      }
                }
                if (mightBeObject && mightBeArray) guessMatch += '|';
                if (mightBeObject) {
                    var guessObjExpr = 'm[' + guessIndex + ']';
                    guessMatch += '(';
                    guessIndex++;
//                    if (prefixChar) guessMatch += regexEscape(prefixChar);
                    // First pair
                    if (escape) {
                        guessMatch += urlPairMatch;
                        guessIndex += 1;
                    } else {
                        guessMatch += '.*' + greedyChar;
                    }
                    guessMatch += '(' + regexEscape(joinChar);
                    guessIndex++;
                    // More pairs
                    if (escape) {
                        guessMatch += urlPairMatch;
                        guessIndex += 1;
                    } else {
                        guessMatch += '.*' + greedyChar;
                    }
                    guessMatch += ')*';
                    guessMatch += ')';

                    callExpr += (callExpr ? '||' : '') + 'p(0,' + guessObjExpr + '||""'
                        + ',0'
                        + ',' + JSON.stringify(varGroupJoinChar)
                    + ')';
                }
                if (mightBeObject && mightBeArray) {
                    guessMatch += ')';
                }

                var justVarName = varName.split(':')[0];
                if (guessReturn[justVarName]) {
                    callExpr += '||' + guessReturn[justVarName];
                }
                guessReturn[justVarName] = callExpr;
            });
            guessMatch += ')?';
            if (fillGroup.length > 1) {
                fillReturn.push('j([' + fillGroup + '],0,' + JSON.stringify(prefixChar) + ',' + JSON.stringify(varGroupJoinChar) + ')');
            } else {
                fillReturn.push(fillGroup[0]);
            }

            if (textPart) {
                fillReturn.push(JSON.stringify(textPart));
                guessMatch += regexEscape(textPart);
            }
        }

        var guessObjParts = [];
        for (var key in guessReturn) {
            guessObjParts.push(JSON.stringify(key) + ':' + guessReturn[key]);
        }
        var guessObj = '{' + guessObjParts + '}';
     
        return {
            mFill: fillReturn.join('+') || '""',
            mFromUriObj: guessReturn,
            mFromUri: '(m=/^' + guessMatch + '$/.exec(u))&&' + guessObj,
            mGuess: '/^' + guessMatch + '$/.exec(u)',
         mTestReg: '/^' + guessMatch.replace(/\(\(([^()]+)\)\)/, '($1)') + '$/'
        };
    }

    function UriTemplate(spec, options) {
        if (!(this instanceof UriTemplate)) return new UriTemplate(spec, options);
        var thisTemplate = this;
        var opts = options || {};

        var prefixes = commonPrefix(opts);
        var fillCode = prefixes.mFill;
        var guessCode = prefixes.mFromUri;

        var returnCodes = returnCode(spec, opts);

        fillCode += 'return';
        if (returnCodes.mFill[0] !== '"') {
            fillCode += ' ';
        }
        fillCode += returnCodes.mFill;
        fillCode += '}';

        guessCode += 'return' + returnCodes.mFromUri + '}';

        var testCode = 'function(u){'
        testCode +=     'return' + returnCodes.mTestReg + '.test(u)';
        testCode += '}';
     
        thisTemplate.fill = evalFunc('(' + (thisTemplate.fillCode = fillCode) + ')');
        thisTemplate.fromUri = evalFunc('(' + (thisTemplate.fromUriCode = guessCode) + ')');
        thisTemplate.test = evalFunc('(' + (thisTemplate.testCode = testCode) + ')');

        thisTemplate.template = spec;
        thisTemplate.toString = function () {
            return spec;
        };
    }

    function Map(spec, opts) {
        if (!(this instanceof Map)) return new Map(spec);
        var thisMap = this;
        var options = {object: true, fixedTypes: opts && opts.fixedTypes};
        var prefixes = commonPrefix(spec, options);
        var forwardCode = '', reverseCode = '';

        [].concat(spec).forEach(function (map) {
            for (var fromSpec in map) {
                var toSpec = map[fromSpec];
                var fromCode = returnCode(fromSpec, options);
                var toCode = returnCode(toSpec, options, function fillExpr(name) {
                    var code = fromCode.mFromUriObj[name];
                    return code || 'null';
                });
                var fromCode = returnCode(fromSpec, options, function fillExpr(name) {
                    var code = toCode.mFromUriObj[name];
                    return code || 'null';
                });

                forwardCode += '(m=' + fromCode.mGuess + ')?' + toCode.mFill + ':';
                reverseCode += '(m=' + toCode.mGuess + ')?' + fromCode.mFill + ':';
            }
        });

        var mapCode = '(function(){' + prefixes.mCommon;
        mapCode += 'return{forward:function(u){return ' + forwardCode + 'null},';
        mapCode += 'reverse:function(u){return ' + reverseCode + 'null}}';
        mapCode += '})()';

        forwardCode = prefixes.mMap + 'return ' + forwardCode + 'null}';
        reverseCode = prefixes.mMap + 'return ' + reverseCode + 'null}';
        thisMap.forward = evalFunc('(' + (thisMap.forwardCode = forwardCode) + ')');
        thisMap.reverse = evalFunc('(' + (thisMap.reverseCode = reverseCode) + ')');
        thisMap.mapCode = mapCode;
    }
    UriTemplate.Map = Map;

    return UriTemplate;
});
